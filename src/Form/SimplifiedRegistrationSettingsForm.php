<?php

namespace Drupal\simplified_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simplified Registration settings form class.
 */
class SimplifiedRegistrationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simplified_registration_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Load default settings.
    $config = $this->config('simplified_registration.settings');

    // Enable birth year field.
    $form['enable_birth_year'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable birth year'),
      '#default_value' => $config->get('simplified_registration.enable_birth_year'),
      '#description' => $this->t('Show birth year field and require it.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('simplified_registration.settings');
    $config->set('simplified_registration.enable_birth_year', $form_state->getValue('enable_birth_year'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simplified_registration.settings',
    ];
  }

}
