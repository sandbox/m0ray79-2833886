<?php

namespace Drupal\simplified_registration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Simplified Registration form class.
 */
class SimplifiedRegistrationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simplified_registration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simplified_registration.settings');

    // User name field.
    $form['user_name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Real name:'),
      '#description' => $this->t('Please enter your full name.'),
    );

    // Birth year field (if enabled).
    if ($config->get('simplified_registration.enable_birth_year')) {
      $form['birth_year'] = array(
        '#type' => 'number',
        '#size' => 4,
        '#maxlength' => 4,
        '#required' => TRUE,
        '#title' => $this->t('Birth year:'),
        '#description' => $this->t('Please enter your birth year (4 digits).'),
      );
    }
    // Submit button.
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Register'),
      '#button_type' => 'primary',
    );

    return $form;

  }

  /**
   * Removes all unneeded spaces from given string and splits it into words.
   *
   * @param string $user_name
   *   The string from user.
   *
   * @return array
   *   Word array.
   */
  protected function sanitizeUserName($user_name) {
    $user_name = preg_replace('/\s+/', ' ', $user_name);
    return explode(' ', trim($user_name));
  }

  /**
   * Transliterate every word in the array.
   *
   * @param array $uname_arr
   *   Word array.
   *
   * @return array
   *   Transliterated word array.
   */
  protected function transliterateUserName(array $uname_arr) {
    $trans = \Drupal::transliteration();
    foreach ($uname_arr as $k => $word) {
      $trans_arr[$k] = $trans->transliterate($word);
    }
    return $trans_arr;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('simplified_registration.settings');
    $user_name = $this->transliterateUserName($this->sanitizeUserName($form_state->getValue('user_name')));
    if (count($user_name) < 2) {
      $form_state->setErrorByName('user_name', $this->t('The username you entered does not look like real name. Please enter at least two words.'));
    }
    $user_name_str = implode('-', $user_name);
    if (strlen($user_name_str) > 120) {
      $form_state->setErrorByName('user_name', $this->t('The username you entered is too long. Please remove or abbreviate some fragments.'));
    }
    if ($config->get('simplified_registration.enable_birth_year')) {
      if (!preg_match('/^\d{4}$/', $form_state->getValue('birth_year'))) {
        $form_state->setErrorByName('birth_year', $this->t('The birth year is invalid. It must contain exactly 4 digits.'));
      }
      $birth_year = intval($form_state->getValue('birth_year'));
      $current_year = intval(date('Y'));
      // World record is 122 years (2016)
      if ($birth_year < $current_year - 130 || $birth_year > $current_year) {
        $form_state->setErrorByName('birth_year', $this->t('The birth year is invalid. Person can not be alive in our days.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('simplified_registration.settings');
    $password = user_password();
    $user_name_sane = $this->sanitizeUserName($form_state->getValue('user_name'));
    $user_name_trans = $this->transliterateUserName($user_name_sane);
    $user_name_str = implode('-', $user_name_trans);
    if ($config->get('simplified_registration.enable_birth_year')) {
      $user_name_str .= '-' . $form_state->getValue('birth_year');
    }
    $user = User::create();
    $user->setPassword($password);
    $user->enforceIsNew();
    $user->setUsername($user_name_str);
    $user->activate();
    $i = 0;
    do {
      try {
        $user->save();
        $success = TRUE;
      }
      catch (\Exception $e) {
        $success = FALSE;
        $i++;
        $user->enforceIsNew();
        $user->setUsername($user_name_str . '-' . $i);
        continue;
      }
    } while (!$success);
    $_SESSION['simplified_registration']['username'] = $user->getUsername();
    $_SESSION['simplified_registration']['password'] = $password;
    user_login_finalize($user);
    $form_state->setRedirect('simplified_registration.registration_complete');
  }

}
