<?php

namespace Drupal\simplified_registration\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for Simplified Registration pages.
 */
class SimplifiedRegistrationController extends ControllerBase {

  /**
   * Show @Registration complete" page.
   */
  public function registrationComplete() {

    $page = array(
      '#theme' => 'registration_complete',
      '#username' => $_SESSION['simplified_registration']['username'],
      '#password' => $_SESSION['simplified_registration']['password'],
    );

    unset($_SESSION['simplified_registration']);

    return $page;
  }

}
