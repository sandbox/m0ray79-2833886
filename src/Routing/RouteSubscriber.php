<?php

namespace Drupal\simplified_registration\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change path '/user/register' to '/user/simplified_registration'.
    if ($route = $collection->get('user.register')) {
      $route->setPath('/user/simplified_registration');
    }
  }

}
