Simplified Registration
=======================

Motivation
----------
This Drupal issue (feature request) is open for more than 8 years.
https://www.drupal.org/node/286401

Many developers and users need a really simple user registration form that can
easily be used by children and older people that have almost no computer and
internet experience.

When I needed it too, I wrote it. Reading Drupal code, I noticed that email is
not required when user already exists and have no email...

Description
-----------
On registartion, user gets a form that have one or two fields: a real name and
birth year (if enabled in module configuration). And a button to submit
this form.

User name is transliterated, then optionally combined with birth year. If such
user exists, a numeric suffix is added. Password is generated automatically.

Then all these values are shown to user with instruction to write them down and
keep in a safe place. User is also activated and logged in.

Simple. Working.

CAPTCHA note
------------
You may need to register CAPTCHA point. Form ID: "simplified_registration_form".

ATTENTION!
----------
Absence of email in user account records can break some functionality. Do not
enable user email confirmation or other email-bound features and modules.

It also lowers security, because users are instantly activated. You may need to
add a role for approved users and revoke some permissions from generic
authenticated users.
